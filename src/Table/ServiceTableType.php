<?php

namespace App\Table;

use App\Entity\Service;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;
use Omines\DataTablesBundle\Column\TextColumn;
use Omines\DataTablesBundle\Column\BoolColumn;
use Omines\DataTablesBundle\Column\DateTimeColumn;
use Omines\DataTablesBundle\Column\NumberColumn;
use Omines\DataTablesBundle\DataTableFactory;
use Omines\DataTablesBundle\DataTable;
use Omines\DataTablesBundle\DataTableTypeInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ServiceTableType extends AbstractController implements DataTableTypeInterface
{

    public function configure(DataTable $dataTable, array $options){
        $dataTable
 		->add('id', NumberColumn::class)
		->add('code', TextColumn::class)
		->add('libelle', TextColumn::class)
		->add('chemin', TextColumn::class)
		->add('actif', BoolColumn::class, [
                    'trueValue' => $options['translator']->trans('Yes'),
                    'falseValue' => $options['translator']->trans('No'),
                    'nullValue' => $options['translator']->trans('Unknown'),
                ])
		->add('createdAt', DateTimeColumn::class, [
                    'format' => 'd-m-Y H:i',
                    'searchable' => false,
                ])
		->add('updatedAt', DateTimeColumn::class, [
                    'format' => 'd-m-Y H:i',
                    'searchable' => false,
                ])
        ->add('link', TextColumn::class, [
            'data' => function (Service $service) use ($options)  {
                return sprintf('<a class="btn btn-primary btn-xs" href="%s"><i class="fa fa-search"></i>  '.$options['translator']->trans('Show').'</a>',
                    $this->generateUrl('service_show', ['id' => $service->getId()])
                );
            },
            'raw' => true
        ])
        ->createAdapter(ORMAdapter::class, [
            'entity' => Service::class,
        ]);
    }
}