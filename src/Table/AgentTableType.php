<?php

namespace App\Table;

use App\Entity\Agent;
use Doctrine\ORM\QueryBuilder;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;
use Omines\DataTablesBundle\Column\NumberColumn;
use Omines\DataTablesBundle\Column\TextColumn;
use Omines\DataTablesBundle\Column\BoolColumn;
use Omines\DataTablesBundle\Column\DateTimeColumn;
use Omines\DataTablesBundle\DataTableFactory;
use Omines\DataTablesBundle\DataTable;
use Omines\DataTablesBundle\DataTableState;
use Omines\DataTablesBundle\DataTableTypeInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AgentTableType extends AbstractController implements DataTableTypeInterface
{

    public function configure(DataTable $dataTable, array $options){
        $dataTable
 		->add('id', NumberColumn::class)
		->add('code', TextColumn::class)
		->add('nom', TextColumn::class)
		->add('prenom', TextColumn::class)
		->add('mail', TextColumn::class)
		->add('actif', BoolColumn::class, [
                    'trueValue' => $options['translator']->trans('Yes'),
                    'falseValue' => $options['translator']->trans('No'),
                    'nullValue' => $options['translator']->trans('Unknown'),
                ])
		->add('createdAt', DateTimeColumn::class, [
                    'format' => 'd/m/Y H:i',
                    'searchable' => false,
                ])
		->add('updatedAt', DateTimeColumn::class, [
                    'format' => 'd/m/Y H:i',
                    'searchable' => false,
                ])
        ->add('link', TextColumn::class, [
            'data' => function (Agent $agent) use ($options)  {
                return sprintf('<a class="btn btn-primary btn-xs" href="%s"><i class="fa fa-search"></i>  '.$options['translator']->trans('Show').'</a>',
                    $this->generateUrl('agent_show', ['id' => $agent->getId()])
                );
            },
            'raw' => true
        ])
        ->createAdapter(ORMAdapter::class, [
            'entity' => Agent::class,
        ]);
    }
}