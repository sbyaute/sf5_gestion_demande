<?php

namespace App\DataFixtures;

use App\Entity\Agent;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker;

class AgentFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $datetime = new \DateTime();
        $min = strtotime('2020/01/01');
        $max = strtotime('2020/12/31');

        $faker = Faker\Factory::create('fr_FR');

        // create 200 agents! Bam!
        for ($i = 1; $i <= 200; $i++) {
            $agent = new Agent();
            $agent->setCode(sprintf('%05s', $i));
            $agent->setNom($faker->lastName);
            $agent->setPrenom($faker->firstName);
            $agent->setMail($faker->email);
            $agent->setActif($faker->boolean);

            $rand = rand($min, $max);
            $agent->setCreatedAt($datetime->createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s', $rand)));
            $agent->setUpdatedAt($datetime->createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s', rand($rand, $max))));

            $agent->setServices($this->getReference('service_'.rand(1,20)));

            $manager->persist($agent);
        }
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            ServiceFixtures::class,
        ];
    }
}
