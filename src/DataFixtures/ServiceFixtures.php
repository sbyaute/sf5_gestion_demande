<?php

namespace App\DataFixtures;

use App\Entity\Service;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ServiceFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $datetime = new \DateTime();
        $min = strtotime('2020/01/01');
        $max = strtotime('2020/12/31');

        // create 20 services! Bam!
        for ($i = 1; $i <= 20; $i++) {
            $service = new Service();
            $service->setCode(sprintf('%05s', $i));
            $service->setLibelle('service_'.$i);
            $service->setChemin('DG/xxxx/SE'.$i);
            $service->setActif('true');

            $rand = rand($min, $max);
            $service->setCreatedAt($datetime->createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s', $rand)));
            $service->setUpdatedAt($datetime->createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s', rand($rand, $max))));

            $manager->persist($service);

            $this->addReference('service_' . $i, $service);

        }
        $manager->flush();
    }
}
