<?php
namespace App\EventSubscriber;

use KevinPapst\AdminLTEBundle\Event\KnpMenuEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Security;


class KnpMenuBuilderSubscriber implements EventSubscriberInterface
{
    private $security;
    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KnpMenuEvent::class => ['onSetupMenu', 100],
        ];
    }

    public function onSetupMenu(KnpMenuEvent $event)
    {
        $menu = $event->getMenu();

        $menu->addChild('mainNavigationMenuItem', [
                'label' => 'Mon Label',
                'childOptions' => $event->getChildOptions()]
        )->setAttribute('class', 'header');

        $menu->addChild('agent', [
                'route' => 'agent_index',
                'label' => 'agent',
                'childOptions' => $event->getChildOptions()]
        )->setLabelAttribute('icon', 'fas fa-user');

        $menu->addChild('service', [
                'route' => 'service_index',
                'label' => 'service',
                'childOptions' => $event->getChildOptions()]
        )->setLabelAttribute('icon', 'fas fa-user');

        $menu->addChild('mainNavigationMenuItem', [
                'label' => 'Admin',
                'childOptions' => $event->getChildOptions()]
        )->setAttribute('class', 'header');

        $menu->addChild('admin', [
                'route' => 'admin',
                'label' => 'Administration',
                'childOptions' => $event->getChildOptions()]
        )->setLabelAttribute('icon', 'fas fa-user');
//
//        if ($this->security->isGranted('ROLE_ADMIN')) {
//            $menu->addChild('administration', [
////                'route' => 'homepage',
//                    'label' => 'Administration',
//                    'childOptions' => $event->getChildOptions()]
//            )->setAttribute('class', 'header');
//
//            $menu->addChild('admin_action', [
////                'route' => 'homepage',
//                    'label' => 'Mon action admin',
//                    'childOptions' => $event->getChildOptions()]
//            )->setLabelAttribute('icon', 'fas fa-cogs');
//        }
    }
}