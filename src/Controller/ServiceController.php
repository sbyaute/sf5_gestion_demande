<?php

namespace App\Controller;

use App\Entity\Service;
use App\Form\ServiceType;
use App\Table\ServiceTableType;
use App\Repository\ServiceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Doctrine\ORM\EntityManagerInterface;
use Omines\DataTablesBundle\DataTableFactory;

/**
 * @Route("/service")
 */
class ServiceController extends AbstractController
{
    /**
     * @Route("/", name="service_index", methods={"GET", "POST"})
     */
    public function index(Request $request, DataTableFactory $dataTableFactory, TranslatorInterface $translator, EntityManagerInterface $em, ServiceRepository $serviceRepository): Response
    {
        $table = $dataTableFactory->createFromType(ServiceTableType::class, [
            'translator' => $translator
        ])->handleRequest($request);

        if ($table->isCallback()) {
            return $table->getResponse();
        }

        // new modal form
        $service = new Service();
        $form = $this->createForm(ServiceType::class, $service);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em->persist($service);
                $em->flush();
            } catch (\Throwable $th) {
                $this->addFlash('error', $translator->trans('Error saving item'));
                return $this->redirectToRoute('service_index');
            }
                $this->addFlash('success', $translator->trans('Item successfully registered'));
                return $this->redirectToRoute('service_index');
        }

        return $this->render('service/index.html.twig', [
            'services' => $serviceRepository->findAll(),
            'datatable' => $table,
            'modal' => [
                'service' => $service,
                'form'  => $form->createView(),
                'title' => $translator->trans('Create new item'),
                'footer' => '
                <div class="pull-right"><button type="submit" class="btn btn-primary" href="'.$this->generateUrl('service_index').'"><i class="fa fa-plus-square"></i> '.$translator->trans('Create new').'</button></div>
                <button type="button" class="btn btn-default" data-dismiss="modal">'.$translator->trans('Close').'</button>
                ',
            ]
        ]);
    }



    /**
     * @Route("/{id}", name="service_show", methods={"GET","POST"})
     */
    public function show(Service $service, Request $request, TranslatorInterface $translator, EntityManagerInterface $em): Response
    {
        // Edit modal form
        $form = $this->createForm(ServiceType::class, $service);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em->flush();
            } catch (\Throwable $th) {
                $this->addFlash('error', $translator->trans('Error editing item'));
                return $this->redirectToRoute('service_show',['id'=>$service->getId()]);
            }
            $this->addFlash('success', $translator->trans('Item successfully edited'));
            return $this->redirectToRoute('service_index');
        }
        return $this->render('service/show.html.twig', [
            'service' => $service,
            'modal' => [
                'service' => $service,
                'form'  => $form->createView(),
                'title' => $translator->trans('Edit item'),
                'footer' => '
                <div class="pull-right"><button type="submit" class="btn btn-primary" href="'.$this->generateUrl('service_show',['id'=>$service->getId()]).'"><i class="fa fa-save"></i> '.$translator->trans('Save').'</button></div>
                <button type="button" class="btn btn-default" data-dismiss="modal">'.$translator->trans('Close').'</button>
                ',
            ],
            'modalDelete' => [
            'title' => $translator->trans('Delete item'),
            'message' => '<p><br>'.$translator->trans('Are you sure you want to delete this item').' ?<br></p>',
            ],
        ]);
    }


    /**
     * @Route("/{id}/delete/{token}", name="service_delete", methods={"GET"})
     */
    public function delete(Request $request, Service $service, $token, EntityManagerInterface $em, TranslatorInterface $translator): Response
    {
        if ($this->isCsrfTokenValid('delete'.$service->getId(), $token)) {
            try {
                $em->remove($service);
                $em->flush();
            } catch (\Throwable $th) {
                $this->addFlash('error', $translator->trans('Error deleting item'));
                return $this->redirectToRoute('service_index');
            }
            $this->addFlash('success', $translator->trans('Item successfully deleted'));
            return $this->redirectToRoute('service_index');            
        }

        $this->addFlash('error', $translator->trans('Error deleting item'));
        return $this->redirectToRoute('service_index');
    }
}
