<?php

namespace App\Controller;

use App\Entity\Agent;
use App\Form\AgentType;
use App\Table\AgentTableType;
use App\Repository\AgentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Doctrine\ORM\EntityManagerInterface;
use Omines\DataTablesBundle\DataTableFactory;

/**
 * @Route("/agent")
 */
class AgentController extends AbstractController
{
    /**
     * @Route("/", name="agent_index", methods={"GET", "POST"})
     */
    public function index(Request $request, DataTableFactory $dataTableFactory, TranslatorInterface $translator, EntityManagerInterface $em, AgentRepository $agentRepository): Response
    {
        $table = $dataTableFactory->createFromType(AgentTableType::class, [
            'translator' => $translator
        ])->handleRequest($request);

        if ($table->isCallback()) {
            return $table->getResponse();
        }

        // new modal form
        $agent = new Agent();
        $form = $this->createForm(AgentType::class, $agent);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em->persist($agent);
                $em->flush();
            } catch (\Throwable $th) {
                $this->addFlash('error', $translator->trans('Error saving item'));
                return $this->redirectToRoute('agent_index');
            }
                $this->addFlash('success', $translator->trans('Item successfully registered'));
                return $this->redirectToRoute('agent_index');
        }

        return $this->render('agent/index.html.twig', [
            'agents' => $agentRepository->findAll(),
            'datatable' => $table,
            'modal' => [
                'agent' => $agent,
                'form'  => $form->createView(),
                'title' => $translator->trans('Create new item'),
                'footer' => '
                <div class="pull-right"><button type="submit" class="btn btn-primary" href="'.$this->generateUrl('agent_index').'"><i class="fa fa-plus-square"></i> '.$translator->trans('Create new').'</button></div>
                <button type="button" class="btn btn-default" data-dismiss="modal">'.$translator->trans('Close').'</button>
                ',
            ]
        ]);
    }



    /**
     * @Route("/{id}", name="agent_show", methods={"GET","POST"})
     */
    public function show(Agent $agent, Request $request, TranslatorInterface $translator, EntityManagerInterface $em): Response
    {
        // Edit modal form
        $form = $this->createForm(AgentType::class, $agent);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em->flush();
            } catch (\Throwable $th) {
                $this->addFlash('error', $translator->trans('Error editing item'));
                return $this->redirectToRoute('agent_show',['id'=>$agent->getId()]);
            }
            $this->addFlash('success', $translator->trans('Item successfully edited'));
            return $this->redirectToRoute('agent_index');
        }
        return $this->render('agent/show.html.twig', [
            'agent' => $agent,
            'modal' => [
                'agent' => $agent,
                'form'  => $form->createView(),
                'title' => $translator->trans('Edit item'),
                'footer' => '
                <div class="pull-right"><button type="submit" class="btn btn-primary" href="'.$this->generateUrl('agent_show',['id'=>$agent->getId()]).'"><i class="fa fa-save"></i> '.$translator->trans('Save').'</button></div>
                <button type="button" class="btn btn-default" data-dismiss="modal">'.$translator->trans('Close').'</button>
                ',
            ],
            'modalDelete' => [
            'title' => $translator->trans('Delete item'),
            'message' => '<p><br>'.$translator->trans('Are you sure you want to delete this item').' ?<br></p>',
            ],
        ]);
    }


    /**
     * @Route("/{id}/delete/{token}", name="agent_delete", methods={"GET"})
     */
    public function delete(Request $request, Agent $agent, $token, EntityManagerInterface $em, TranslatorInterface $translator): Response
    {
        if ($this->isCsrfTokenValid('delete'.$agent->getId(), $token)) {
            try {
                $em->remove($agent);
                $em->flush();
            } catch (\Throwable $th) {
                $this->addFlash('error', $translator->trans('Error deleting item'));
                return $this->redirectToRoute('agent_index');
            }
            $this->addFlash('success', $translator->trans('Item successfully deleted'));
            return $this->redirectToRoute('agent_index');            
        }

        $this->addFlash('error', $translator->trans('Error deleting item'));
        return $this->redirectToRoute('agent_index');
    }
}
