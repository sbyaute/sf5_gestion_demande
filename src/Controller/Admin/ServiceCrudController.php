<?php

namespace App\Controller\Admin;

use App\Entity\Service;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ServiceCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Service::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('code'),
            TextField::new('libelle'),
            TextField::new('chemin'),
            BooleanField::new('actif'),
            DateTimeField::new('createdAt')->setFormat('d/M/y H:m')->hideOnForm()->setLabel('Créé le'),
            DateField::new('updatedAt')->setFormat('d/M/y H:m')->hideOnForm()->setLabel('Mis à jour le'),
        ];
    }

}
