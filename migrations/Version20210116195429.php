<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210116195429 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE orga_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE orga (
                id INT NOT NULL, 
                libelle VARCHAR(100) NOT NULL, 
                caisse VARCHAR(3) NOT NULL, 
                ucanss VARCHAR(6) NOT NULL, 
                siret VARCHAR(19) NOT NULL, 
                perso VARCHAR(100) DEFAULT NULL, 
                actif BOOLEAN NOT NULL, 
                PRIMARY KEY(id),
                CONSTRAINT orga_uk1 UNIQUE (libelle),
                CONSTRAINT orga_uk2 UNIQUE (caisse),
                CONSTRAINT orga_uk3 UNIQUE (ucanss),
                CONSTRAINT orga_uk4 UNIQUE (siret) 
              )
        ');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE orga_id_seq CASCADE');
        $this->addSql('DROP TABLE orga');
    }
}
